Rails.application.routes.draw do

 devise_for :users, :path_prefix => 'my'
  resources :users do 
    put :confirm, :on => :collection
    resources :skills
    resources :works
  end 

  root 'pages#index'

  get 'connections', to: 'users#index'

  resources :friendships, only: [:create, :destroy, :accept] do 
    member do 
      put :accept
    end
  end

  resources :startups do 
    put :join, :on => :collection
    put :wishlist, :on => :collection
  end 
  resources :interests, only: [:destroy]
  get 'interests', to: 'interests#index'
  get 'startups', to: 'startups#index'

    resources :skills
    resources :works
    resources :conversations
    resources :personal_messages
end
