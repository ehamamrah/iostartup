class UsersController < ApplicationController
  before_action :authenticate_user!, only: [:show, :index]
	before_action :set_user, only:[:show]
  before_action :get_counts, only: [:index]

	def index
		case params[:people] when "friends"
      @users = current_user.active_friends
    when "requests"
      @users = current_user.pending_friend_requests_from.map(&:user)
    when "pending"
      @users = current_user.pending_friend_requests_to.map(&:friend)
    else
      @users = User.where.not(id: current_user.id)
    end
	end

  def show
      @activities = PublicActivity::Activity.where(owner_id: @user.id)
      @startups = @user.startups.all.order("created_at DESC")
      @joinrequests = []
        current_user.startups.each do |startup|
          @joinrequests << startup.teams.where(status: "pending")
        end
  end

    def confirm 
        @request = Team.find_by(user_id: params[:id])
        @request.status = "confirmed"
        if @request.save
            redirect_to root_path, notice: "You Accepted new member in your team"
        end 
    end 

   
  # GET /users/new
  # GET /users/new.json
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
          @skills = current_user.skills.all
          @works = current_user.works.all
  end

  # POST /users
  # POST /users.json
  def create
      @user = User.new(user_params)
      if @user.save
         redirect_to root_path, :notice => 'User was successfully created.' 
      else
        render 'new'
      end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
      @user = User.find(params[:id])
      if @user.update(user_params)
        redirect_to edit_user_path(@user), :notice => " #{@user.fullname} details was successfully updated. " 
      else
       render 'edit'
      end
  end

  private

  def get_counts
    @friend_count = current_user.active_friends.size
    @pending_count = current_user.pending_friend_requests_to.map(&:friend).size
  end

  def set_user
  	@user = User.find_by(username: params[:id])
  end

  def user_params 
    	params.require(:user).permit(:email, :password, :password_confirmation, :username, :bio, :age,
		 :gender, :avatar, :fullname, :designation)
  end 
  
end