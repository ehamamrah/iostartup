class InterestsController < ApplicationController

  before_action :find_interest, only: [:destroy]

  def index
    @interests = Interest.all.where(user_id: current_user.id).order('created_at DESC')
  end

  def destroy
    @interest.destroy 
    redirect_to interests_path, notice: 'Startup interest removed from your list'
  end 
  

  private 
  def intertest_params 
    params.require(:interest).permit(:startup_id, :user_id)
  end

  def find_interest
    @interest = Interest.find(params[:id])
  end 
  
  
end
