class StartupsController < ApplicationController 
    before_action :authenticate_user!
    before_action :find_startup, only: [:show, :join, :wishlist]

    def index 
        @startups = Startup.all.order("created_at DESC")
    end 

    def new
        @startup = current_user.startups.build
    end 

    def create 
        @startup = current_user.startups.build(startup_params)
        if @startup.save 
            redirect_to root_path, notice: 'Startup Submited'
        else 
            render 'new'
        end 
    end 

    def show 
        @members = @startup.teams.all.where(status: 'confirmed')
        @wish = Interest.where(startup_id: @startup.id, user_id: current_user.id)
        @inteam = Team.where(startup_id: @startup.id, user_id: current_user.id)
    end 
    
    def join 
        @request = Team.new
        @request.startup_id = @startup.id
        @request.user_id = current_user.id
        @request.status = "pending"
        if @request.save 
            @user = current_user
            @user.team_id = @request.id 
            @user.save
            redirect_to @startup, notice: "Joining Request Sent"
        else 
            redirect_to @startup, notice: "Error !! Try again later"
        end 
    end 
    
    def wishlist 
        @wish = Interest.new
        @wish.startup_id = @startup.id 
        @wish.user_id = current_user.id 
        if @wish.save 
            redirect_to @startup, notice: "You added #{@startup.name} to your interest list"
        else 
            redirect_to @startup, notice: "Error !! Try again later"
        end 
    end 
    
    
    private
    def startup_params
        params.require(:startup).permit(:name, :desc, :funded_date, :logo, :user_id)
    end 

    def find_startup 
        @startup = Startup.friendly.find(params[:id])
    end 
end 
