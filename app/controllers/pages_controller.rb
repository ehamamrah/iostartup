class PagesController < ApplicationController

    def index 
        @startups = Startup.all.order("created_at DESC").limit(3)
        @users = User.all.order("created_at DESC").limit(4)
    end 
    
end 
