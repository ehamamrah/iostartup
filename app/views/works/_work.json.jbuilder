json.extract! work, :id, :company_name, :from, :to, :created_at, :updated_at
json.url work_url(work, format: :json)
