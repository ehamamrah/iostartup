module UsersHelper
	def action_buttons(user)
		case current_user.friendship_status(user) when "friends"
			link_to "Terminate Connection", friendship_path(current_user.friendship_relation(user)), method: :delete, class: "btn btn-xs btn-danger"
		when "pending"
			link_to "Terminate Request", friendship_path(current_user.friendship_relation(user)), method: :delete, class: "btn btn-xs btn-warning"
		when "requested"
			link_to "Accept Connection", accept_friendship_path(current_user.friendship_relation(user)), method: :put, class: "btn btn-xs btn-success"
		when "not_friends"
			link_to "Connect", friendships_path(user_id: user.id), method: :post, class: "btn btn-xs btn-primary"
		end
	end
end