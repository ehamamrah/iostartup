class Startup < ApplicationRecord
    extend FriendlyId
    friendly_id :name, use: :slugged
    
    belongs_to :user
    has_many :teams, dependent: :destroy
    has_many :interests, dependent: :destroy
    has_attached_file :logo, styles: { medium: "300x300>", thumb: "75x75>" }, :default_url => ActionController::Base.helpers.asset_path("startup_logo.png")
    validates_attachment_content_type :logo, content_type: /\Aimage\/.*\z/
end
