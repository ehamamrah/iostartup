# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# User.create(username: "tiagom87", email: "a1@gmail.com", password: "112233", password_confirmation: "112233", fullname: "Ahmad Adel")
User.create(username: "tiagom88", email: "a2@gmail.com", password: "112233", password_confirmation: "112233", fullname: "Ahmad Adel")
User.create(username: "tiagom89", email: "a3@gmail.com", password: "112233", password_confirmation: "112233", fullname: "Ahmad Adel")
User.create(username: "tiagom90", email: "a4@gmail.com", password: "112233", password_confirmation: "112233", fullname: "Ahmad Adel")
User.create(username: "tiagom91", email: "a5@gmail.com", password: "112233", password_confirmation: "112233", fullname: "Ahmad Adel")
p "Test users created"