class CreateStartups < ActiveRecord::Migration[5.0]
  def change
    create_table :startups do |t|
      t.string :name
      t.text :desc
      t.datetime :funded_date
      t.string :logo

      t.timestamps
    end
  end
end
