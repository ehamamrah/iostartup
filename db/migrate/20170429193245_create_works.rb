class CreateWorks < ActiveRecord::Migration[5.0]
  def change
    create_table :works do |t|
      t.string :company_name
      t.datetime :from
      t.datetime :to

      t.timestamps
    end
  end
end
