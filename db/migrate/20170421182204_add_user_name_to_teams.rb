class AddUserNameToTeams < ActiveRecord::Migration[5.0]
  def change
    add_column :teams, :username, :string
  end
end
