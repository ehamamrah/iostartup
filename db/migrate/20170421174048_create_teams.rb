class CreateTeams < ActiveRecord::Migration[5.0]
  def change
    create_table :teams do |t|
      t.references :user, foreign_key: true
      t.references :startup, foreign_key: true
      t.string :status, default: 'pending'

      t.timestamps
    end
  end
end
