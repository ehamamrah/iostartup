class AddUserIdToStartups < ActiveRecord::Migration[5.0]
  def change
    add_column :startups, :user_id, :int
  end
end
