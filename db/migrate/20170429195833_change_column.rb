class ChangeColumn < ActiveRecord::Migration[5.0]
   def self.up
     change_table :works do |t|
       t.change :from, :date
       t.change :to, :date
     end
   end 
    
  def self.down
    change_table :works do |t|
      t.change :from, :datetime
      t.change :to, :datetime
    end
  end 
  
end
